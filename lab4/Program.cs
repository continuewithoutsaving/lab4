﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StructConlsole
{

    class Program
    {
        public struct Entrant
        {
            public string Name;
            public long IdNum;
            public int CoursePoints;
            public double AvgPoints;
            public ZNO[] ZNOResults;
            public Entrant(string name, long idNum, int coursePoints, double avgPoints, ZNO[] znoResults)
            {
                Name = name;
                IdNum = idNum;
                CoursePoints = coursePoints;
                AvgPoints = avgPoints;
                ZNOResults = znoResults;
            }

            public void ReadInfo()
            {
                int n;
                bool f = true;
                Console.Write("Введіть прізвище та ініціали абітурієнта:");
                Name = Console.ReadLine();
                do
                {
                    Console.Write("Введіть ідентифікаційний код абітурієнта:");
                    f = true;
                    if (!long.TryParse(Console.ReadLine(), out IdNum))
                    {
                        Console.WriteLine("Помилка!");
                        f = false;
                    }
                } while (f == false);
                do
                {
                    Console.Write("Введіть бали за підготовчі курси:");
                    f = true;
                    if (!int.TryParse(Console.ReadLine(), out CoursePoints))
                    {
                        Console.WriteLine("Помилка!");
                        f = false;
                    }

                } while (f == false || CoursePoints > 10 || CoursePoints < 0);
                do
                {
                    Console.Write("Введіть бал атестату(з 200 балів):");
                    f = true;
                    if (!double.TryParse(Console.ReadLine(), out AvgPoints))
                    {
                        Console.WriteLine("Помилка!");
                        f = false;
                    }

                } while (f == false || AvgPoints > 200 || AvgPoints < 1);
                do
                {
                    Console.Write("Введіть кількість предметів для здачі ЗНО:");
                    f = true;
                    if (!int.TryParse(Console.ReadLine(), out n))
                    {
                        Console.WriteLine("Помилка!");
                        f = false;
                    }

                } while (f == false || n > 4 || n < 3);
                ZNOResults = new ZNO[n];
                for (int i = 0; i < ZNOResults.Length; i++)
                {
                    Console.Write("Введіть предмет:");
                    ZNOResults[i].Subject = Console.ReadLine();
                    do
                    {
                        Console.Write("Введіть оцінку:");
                        f = true;
                        if (!double.TryParse(Console.ReadLine(), out ZNOResults[i].Points))
                        {
                            Console.WriteLine("Помилка!");
                            f = false;
                        }

                    } while (f == false || ZNOResults[i].Points > 200 || ZNOResults[i].Points < 1);
                }
            }
            public void PrintEntrant()
            {
                char a = ' ';
                Console.Write("|{0,-17}|{1,-12}|{2,-9}|{3,-12}|", Name, IdNum, CoursePoints, AvgPoints);
                if (ZNOResults.Length == 3)
                {

                    for (int i = 0; i < ZNOResults.Length; i++)
                    {
                        if (ZNOResults[i].Points < 100)
                        {
                            Console.Write("{0,-10}|н/c|", ZNOResults[i].Subject);
                        }
                        else Console.Write("{0,-10}|{1,-3}|", ZNOResults[i].Subject, ZNOResults[i].Points);
                    }
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write("{0,-10}|{0,-3}|\n", a);
                    Console.ResetColor();
                }
                else
                {
                    for (int i = 0; i < ZNOResults.Length; i++)
                    {
                        if (ZNOResults[i].Points < 100)
                        {
                            Console.Write("{0,-10}|н/c|", ZNOResults[i].Subject);
                        }
                        else Console.Write("{0,-10}|{1,-3}|", ZNOResults[i].Subject, ZNOResults[i].Points);
                    }
                    Console.WriteLine("");

                }
            }
            public double GetCompMark()
            {
                double mark;
                if (ZNOResults[0].Points >= 100 && ZNOResults[1].Points >= 100 && ZNOResults[2].Points >= 100)
                {
                    mark = AvgPoints * 0.1 + 0.05 * CoursePoints + 0.25 * ZNOResults[0].Points + 0.4 * ZNOResults[1].Points + 0.2 * ZNOResults[2].Points;
                    return mark;
                }
                else if (ZNOResults[0].Points < 100 && ZNOResults[1].Points < 100)
                {
                    mark = AvgPoints * 0.1 + 0.05 * CoursePoints + 0.2 * ZNOResults[2].Points;
                    return mark;
                }
                else if (ZNOResults[1].Points < 100 && ZNOResults[2].Points < 100)
                {
                    mark = AvgPoints * 0.1 + 0.05 * CoursePoints + 0.25 * ZNOResults[0].Points;
                    return mark;
                }
                else if (ZNOResults[0].Points < 100 && ZNOResults[2].Points < 100)
                {
                    mark = AvgPoints * 0.1 + 0.05 * CoursePoints + 0.4 * ZNOResults[1].Points;
                    return mark;
                }
                else if (ZNOResults[0].Points < 100)
                {
                    mark = AvgPoints * 0.1 + 0.05 * CoursePoints + 0.4 * ZNOResults[1].Points + 0.2 * ZNOResults[2].Points;
                    return mark;
                }
                else if (ZNOResults[1].Points < 100)
                {
                    mark = AvgPoints * 0.1 + 0.05 * CoursePoints + 0.25 * ZNOResults[0].Points + 0.2 * ZNOResults[2].Points;
                    return mark;
                }
                else if (ZNOResults[2].Points < 100)
                {
                    mark = AvgPoints * 0.1 + 0.05 * CoursePoints + 0.25 * ZNOResults[0].Points + 0.4 * ZNOResults[1].Points;
                    return mark;
                }
                else return 0;
            }
            public string GetBestSubject()
            {
                double max = -1e7;
                int k = 0;
                for (int i = 0; i < ZNOResults.Length; i++)
                {
                    if (ZNOResults[i].Points > max)
                    {
                        max = ZNOResults[i].Points;
                        k = i;
                    }
                }
                return ZNOResults[k].Subject;
            }
            public string GetWorstSubject()
            {
                double min = 1e7;
                int k = 0;
                for (int i = 0; i < ZNOResults.Length; i++)
                {
                    if (ZNOResults[i].Points < min)
                    {
                        min = ZNOResults[i].Points;
                        k = i;
                    }
                }
                return ZNOResults[k].Subject;
            }
        }

        public struct ZNO
        {
            public string Subject;
            public double Points;
            public ZNO(string subject, double points)
            {
                Subject = subject;
                Points = points;
            }

        }
        public static Entrant[] ent;

        public static void ReadEntrantsArray()
        {
            Array.Resize(ref ent, ent.Length + 1);
            ent[ent.Length - 1] = new Entrant();
            ent[ent.Length - 1].ReadInfo();
        }
        public static void PrintEntrants()
        {
            if (ent.Length == 0)
            {
                Console.WriteLine("Помилка!");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("|Прізвище та ініц.|Ідентиф. код|Дод. бали|Бал атестату| Предмет  |Бал| Предмет  |Бал| Предмет  |Бал| Предмет  |Бал|");
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------");
                Console.ResetColor();
                for (int i = 0; i < ent.Length; i++)
                {
                    ent[i].PrintEntrant();
                }
            }
            Console.WriteLine("\nн/с - не склав");

        }
        public static double GetEntrantsInfo(Entrant[] ent, out double maxbal, out double minbal, out int ki, out int fi)
        {
            double n = 0;
            ki = 0;
            fi = 0;
            double k = 0, f = 0;
            maxbal = -1e7;
            minbal = 1e7;
            for (int i = 0; i < ent.Length; i++)
            {
                k = ent[i].GetCompMark();
                if (k > maxbal)
                {
                    maxbal = k;
                    ki = i;
                }
            }
            for (int j = 0; j < ent.Length; j++)
            {
                f = ent[j].GetCompMark();
                if (f < minbal)
                {
                    minbal = f;
                    fi = j;
                }
            }
            return n;
        }
        public static int SortEntrantsByPoints(Entrant a, Entrant b)
        {

            if (a.GetCompMark() < b.GetCompMark())
            {
                return 1;
            }
            if (a.GetCompMark() > b.GetCompMark())
            {
                return -1;
            }
            return 0;
        }
        public static int SortEntrantsByName(Entrant a, Entrant b)
        {
            if (a.Name.CompareTo(b.Name) > 0)
            {
                return 1;
            }
            if (a.Name.CompareTo(b.Name) < 0)
            {
                return -1;
            }
            if (a.Name == b.Name)
            {
                return SortEntrantsByPoints(a, b);
            }
            return 0;
        }

        static void Main(string[] args)
        {
            Console.Title = "Лабораторна робота №4";
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            Console.SetWindowSize(100, 30);
            bool t = true;

            int num;
            ent = new Entrant[0];

            do
            {
                do
                {
                    Console.Clear();
                    t = true;
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("Меню:");
                    Console.ResetColor();
                    Console.Write("1.Додати запис\n2.Вивести дані\n3.Конкурсний бал\n4.Предмет з найкращим балом абітурієнта\n5.Предмет з найгіршим балом абітурієнта\n6.Найвищий і найнижчий конкурсний бал серед усіх абітурієнтів.\n7.Сортування за спаданням конкурсного балу абітурієнта.\n8.Сортування за прізвищем(від а до я)\n0.Вихід\nВиберіть розділ:");
                    if (!int.TryParse(Console.ReadLine(), out num))
                    {
                        Console.WriteLine("Помилка!");
                        t = false;
                    }
                    if (num > 8)
                    {
                        Console.WriteLine("Помилка!");
                    }
                } while (t == false || num > 8);
                switch (num)
                {
                    case 1:
                        Console.Clear();
                        ReadEntrantsArray();
                        Console.ReadKey();
                        break;
                    case 2: Console.Clear(); PrintEntrants(); Console.ReadKey(); break;
                    case 3:
                        Console.Clear();
                        if (ent.Length == 0)
                        {
                            Console.WriteLine("Помилка!");
                        }
                        else
                        {
                            for (int i = 0; i < ent.Length; i++)
                            {
                                Console.WriteLine("{0} = {1}", ent[i].Name, ent[i].GetCompMark());
                            }
                        }
                        Console.ReadKey();
                        break;
                    case 4:
                        Console.Clear();
                        if (ent.Length == 0)
                        {
                            Console.WriteLine("Помилка!");
                        }
                        else
                        {
                            for (int i = 0; i < ent.Length; i++)
                            {
                                Console.WriteLine("{0} = {1}", ent[i].Name, ent[i].GetBestSubject());
                            }
                        }
                        Console.ReadKey();

                        break;
                    case 5:
                        Console.Clear();
                        if (ent.Length == 0)
                        {
                            Console.WriteLine("Помилка!");
                        }
                        else
                        {
                            for (int i = 0; i < ent.Length; i++)
                            {
                                Console.WriteLine("{0} = {1}", ent[i].Name, ent[i].GetWorstSubject());
                            }
                        }
                        Console.ReadKey();
                        break;
                    case 6:
                        Console.Clear();
                        if (ent.Length == 0)
                        {
                            Console.WriteLine("Помилка!");
                        }
                        else
                        {
                            double n;
                            int fi, ki;
                            double maxbal, minbal;
                            n = GetEntrantsInfo(ent, out maxbal, out minbal, out ki, out fi);
                            Console.WriteLine("Найкращий бал\n{0} = {1}\nНайгірший бал\n{2} = {3}", ent[ki].Name, maxbal, ent[fi].Name, minbal);
                        }
                        Console.ReadKey();
                        break;
                    case 7:
                        Console.Clear();
                        if (ent.Length == 0)
                        {
                            Console.WriteLine("Помилка!");
                        }
                        else if (ent.Length == 1)
                        {
                            Console.WriteLine("Помилка!");
                        }
                        else
                        {
                            Array.Sort(ent, SortEntrantsByPoints);
                            PrintEntrants();
                        }
                        Console.ReadKey();
                        break;
                    case 8:
                        Console.Clear();
                        if (ent.Length == 0)
                        {
                            Console.WriteLine("Помилка!");
                        }
                        else if (ent.Length == 1)
                        {
                            Console.WriteLine("Помилка!");
                        }
                        else
                        {
                            Array.Sort(ent, SortEntrantsByName);
                            PrintEntrants();
                        }
                        Console.ReadKey();
                        break;
                    case 0:; break;
                    default:
                        Console.WriteLine("Помилка!");
                        break;
                }
            } while (num != 0);
        }
       
    }
}